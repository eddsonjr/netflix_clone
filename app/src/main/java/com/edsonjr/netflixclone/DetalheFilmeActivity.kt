package com.edsonjr.netflixclone

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import com.edsonjr.netflixclone.Adapter.FilmesAdapter
import com.edsonjr.netflixclone.Model.addFilmes
import com.edsonjr.netflixclone.databinding.ActivityDetalheFilmeBinding
import com.squareup.picasso.Picasso

class DetalheFilmeActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDetalheFilmeBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetalheFilmeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportActionBar!!.hide()
        toolBar()

        val recycler_outros_filmes = binding.recyclerOutrosFilmes
        recycler_outros_filmes.adapter = FilmesAdapter(addFilmes())
        recycler_outros_filmes.layoutManager = GridLayoutManager(applicationContext,3)


        //pegando o token da url do firebase
        val capaTheWitcher = "https://firebasestorage.googleapis.com/v0/b/netflix-clone-aba9b.appspot.com/o/video.jpg?alt=media&token=d887d472-e900-45d0-8986-053906e91c23"
        Picasso.get().load(capaTheWitcher).fit().into(binding.capa) //carregando a capa do filme


        binding.playVideo.setOnClickListener {

            val intent = Intent(this,VideoActivity::class.java)
            startActivity(intent)

        }

    }


    private fun toolBar(){
        val toolbar_detalhes = binding.toolbarDetalhes
        toolbar_detalhes.setNavigationIcon(getDrawable(R.drawable.ic_voltar))
        toolbar_detalhes.setNavigationOnClickListener {
            val intent = Intent(this,ListaFilmesActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

}