package com.edsonjr.netflixclone

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportActionBar!!.hide() //esconendo da action bar

        //Trabalhando com a animacao e a  transicao entre as activities
        Handler(Looper.getMainLooper()).postDelayed({
            AbrirTelaLogin() //carrega a activity de login
        },2000)

        /*
        * O metodo acima e responsavel por executar um determinado bloco de instrucao apos um
        * determinado limite de tempo. No caso sera carregada a activity de login do aplicativo
        * apos 2000 milisegundos (que equivale a 2 segundos)
        * */
    }


    private fun AbrirTelaLogin(){
        val intent =  Intent(this,FormLoginActivity::class.java)
        startActivity(intent)
        finish()

    }
}