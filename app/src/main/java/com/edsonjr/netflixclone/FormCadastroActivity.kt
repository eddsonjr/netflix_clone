package com.edsonjr.netflixclone

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.edsonjr.netflixclone.databinding.ActivityFormCadastroBinding
import com.google.firebase.FirebaseNetworkException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthUserCollisionException
import com.google.firebase.auth.FirebaseAuthWeakPasswordException

class FormCadastroActivity : AppCompatActivity() {

    private lateinit var binding: ActivityFormCadastroBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFormCadastroBinding.inflate(layoutInflater)
        setContentView(binding.root)

        toolBar()

        //evento de click no botao de cadastrar
        binding.btCadastrar.setOnClickListener {

            val email = binding.editEmail.text.toString()
            val senha = binding.editSenha.text.toString()
            val mensagem_error = binding.mensagemErro

            if(email.isEmpty() || senha.isEmpty()){
                mensagem_error.setText("Preencha todos os campos")
            }else{
                cadastrarUsuario()
            }

        }






    }

    //configura a toolbar
    private fun toolBar() {
        //configurando a action bar via codigo
        supportActionBar!!.hide() //esconda a barra de acao
        val toolbar = binding.toolbarCadastro
        toolbar.setBackgroundColor(getColor(R.color.white))
        toolbar.setNavigationIcon(getDrawable(R.drawable.ic_netflix_official_logo))
    }


    //cadastrando um usuario
    private fun cadastrarUsuario(){

        val email = binding.editEmail.text.toString()
        val senha = binding.editSenha.text.toString()
        val mensagem_error = binding.mensagemErro

        FirebaseAuth.getInstance().createUserWithEmailAndPassword(email,senha).addOnCompleteListener {
            //verifica se o usuario foi cadastrado com sucesso
            if(it.isSuccessful){
                Toast.makeText(this,"Usuário cadastrdo com sucesso!",Toast.LENGTH_LONG).show()

                //limpando os campos de email e senha
                binding.editEmail.setText("")
                binding.editSenha.setText("")
                mensagem_error.setText("")
            }
        }.addOnFailureListener {
            //caso ocorra erro - Tratamento de erros

            var error = it

            when{
                error is FirebaseAuthWeakPasswordException -> mensagem_error.setText("Digite uma senha com no mínimo 6 caracteres") //senha fraca
                error is FirebaseAuthUserCollisionException -> mensagem_error.setText("Esta conta já foi cadastrada!") //conta ja cadastrada e tentando cadastrar ela novamente
                error is FirebaseNetworkException -> mensagem_error.setText("Sem conexão com a internet") //falha de conexao
                else -> mensagem_error.setText("Erro ao cadastrar o usuário. Erro desconhecido")
            }
        }


    }
}