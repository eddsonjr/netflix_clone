package com.edsonjr.netflixclone

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.edsonjr.netflixclone.databinding.ActivityFormLoginBinding
import com.google.firebase.FirebaseNetworkException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException

class FormLoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityFormLoginBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFormLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //Remove a action bar
        supportActionBar!!.hide()


        //vericando se o usuario esta logado
        verificarUsuarioLogado()

        binding.txtTelaCadastro.setOnClickListener{
            val intent = Intent(this,FormCadastroActivity::class.java)
            startActivity(intent)
        }


        //evento de click para o botao de entrar
        binding.btEntrar.setOnClickListener {

            val email = binding.editEmail.text.toString()
            val senha = binding.editSenha.text.toString()
            val mensagem_error = binding.mensagemErro

            if(email.isEmpty() || senha.isEmpty()){ //verificando se os campos estao vazios
                mensagem_error.setText("Preencha todos os campos!")
            }else{ //procedendo com a autenticacao
                autenticarUsuario()
            }
        }



    }

    //metodo para autenticar usuario
    private fun autenticarUsuario(){

        val email = binding.editEmail.text.toString()
        val senha = binding.editSenha.text.toString()
        val mensagem_error = binding.mensagemErro

        //recuperando instancia do firebase
        FirebaseAuth.getInstance().signInWithEmailAndPassword(email,senha).addOnCompleteListener {
            if(it.isSuccessful){ //login com sucesso
                Toast.makeText(this,"Usuário logado com sucesso!",Toast.LENGTH_SHORT).show()

                //carregando a proxima activity
                irParaTelaDeFilmes()
            }

        }.addOnFailureListener {
            //verifica possiveis falhas

            var error = it
            when{
                error is FirebaseAuthInvalidCredentialsException -> mensagem_error.setText("Email ou senha inválidos")
                error is FirebaseNetworkException -> mensagem_error.setText("Sem conexão com a internet")
                else  -> mensagem_error.setText("Erro desconhecido ao logar")
            }

        }


    }


    //verificando se o usuario esta logado
    private fun verificarUsuarioLogado() {
        val usuarioLogado = FirebaseAuth.getInstance().currentUser //pega o usuaro atual logado

        if(usuarioLogado != null){ //se houve um usuario logado (!= null)
            irParaTelaDeFilmes()
        }

        /*Se ja houve um usuario logado no firebase, o aplicativo ira iniciar ja na tela de
        * filmes. */
    }

    private fun irParaTelaDeFilmes(){
        val intent = Intent(this,ListaFilmesActivity::class.java)
        startActivity(intent)
        finish()
    }
}