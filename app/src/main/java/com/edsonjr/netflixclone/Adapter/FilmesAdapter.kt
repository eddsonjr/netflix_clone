package com.edsonjr.netflixclone.Adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.edsonjr.netflixclone.Model.Filmes
import com.edsonjr.netflixclone.databinding.ListaFilmesCellBinding

class FilmesAdapter(val filmes: MutableList<Filmes>):
        RecyclerView.Adapter<FilmesAdapter.FilmesViewHolder>() {


    inner class FilmesViewHolder(val binding: ListaFilmesCellBinding):
            RecyclerView.ViewHolder(binding.root) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FilmesViewHolder {
        val binding = ListaFilmesCellBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return FilmesViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return filmes.size

    }

    override fun onBindViewHolder(holder: FilmesViewHolder, position: Int) {
        with(holder){
            with(filmes[position]){
                binding.capaFilme.setImageResource(capaFilme)
            }
        }
    }
}