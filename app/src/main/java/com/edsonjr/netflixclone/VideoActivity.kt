package com.edsonjr.netflixclone

import android.net.Uri
import android.os.Binder
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.MediaController
import com.edsonjr.netflixclone.databinding.ActivityVideoBinding

class VideoActivity : AppCompatActivity() {

    private lateinit var binding: ActivityVideoBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityVideoBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportActionBar!!.hide()

        //configurando o video player para ele pegar da web
        val videoURL = Uri.parse("https://firebasestorage.googleapis.com/v0/b/netflix-clone-aba9b.appspot.com/o/THE%20WITCHER%20_%20TRAILER%20FINAL%20_%20NETFLIX.mp4?alt=media&token=1ecbc9b5-d41c-4c2a-ba6e-e4983dd60d88 ")
        val video = binding.videoPlayer

        video.setMediaController(MediaController(this))
        video.setVideoURI(videoURL)
        video.requestFocus()
        video.start()

    }
}